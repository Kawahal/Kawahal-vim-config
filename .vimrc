" HTML automatic completion
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags 
"
" Colors
if (has("termguicolors"))
 set termguicolors
endif
syntax enable 
" set background=light
colorscheme basic-dark

" Airline
" remove the filetype part
let g:airline_section_x=''
" remove separators for empty sections
let g:airline_skip_empty_sections = 1
let g:airline_theme='breezy'
let g:airline_detect_iminsert=1
let g:airline_detect_paste=1
let g:airline_detect_paste=1
let g:airline_detect_modified=1
let g:Powerline_symbols = 'fancy'
set laststatus=2

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" Nerdtree at startup
" autocmd VimEnter * NERDTree
" autocmd BufEnter * NERDTreeMirror
" autocmd VimEnter * wincmd w

" Number column at left
set number 

" Line at 80 chars lenght
set colorcolumn=80

"Pathogen plugin loader
execute pathogen#infect()
filetype plugin indent on
syntax on 

" Font and fontsize
" if has("gui_running")                    
"   if has("gui_gtk2") || has("gui_gtk3")
"     set guifont=Courier\ New\ 11
"   elseif has("gui_photon")
"     set guifont=Courier\ New:s11
"   elseif has("gui_kde")
"     set guifont=Courier\ New/11/-1/5/50/0/0/0/1/0
"   elseif has("x11")
"     set guifont=-*-Courier-medium-r-normal-*-*-180-*-*-m-*-*
"   else
"     set guifont=Courier:h11:cDEFAULT
"   endif
" endif

" Close brackets and pharentesis automatically
" inoremap ( ()<C-G>U<Left>
" inoremap { {}<C-G>U<Left>
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

" Tab settings: 
set tabstop=4       " Tabs with width of 4
set shiftwidth=4    " Indents  width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand tabs to spaces

" Other basic options
set encoding=utf-8        " utf-8 encoding 
set showcmd               " count highlighted
set ruler                 " Show placement 
set mouse=a               " Use of mouse
set hidden                " Stash unwritten files in buffer
set vb                    

"set cursorline            " Highlight current line
set scrolloff=3           " Start scrolling when I'm 3 lines from top/bottom
set history=1000          " Remember commands and search history
set backspace=2           " Backspace over indent, eol, and insert
set mousehide             " Hide the mouse pointer while typing
set number                " Show linenumbers
set nowrap                " Turn off linewrap

"set list                  " Show invisible chars like $ at the end of line
set hlsearch              " highlight search
set incsearch             " incremental search
set wrapscan              " Set the search scan to wrap around the file
set ignorecase            " Case ignored when searching
set smartcase             " Not ignoring cases if using an upper case

" Backup settings
set nobackup
set nowritebackup
set noswapfile

"indentation lines
" Char of line
let g:indentLine_char = "│"
let g:indentLine_color_term = 8 
let g:indentLine_color_gui = '#3b3d3c'

" React highlighting in js files
let g:jsx_ext_required = 0

" Conceal level 0 (for working more efficiently with md files)
set conceallevel=0
let g:conceallevel=0

"map jj to return to normal mode
inoremap jj <Esc>

"change cursor appearence depending on the mode
  " solid vertical bar 
  let &t_SI .= "\<Esc>[6 q"
  " solid block
  let &t_EI .= "\<Esc>[2 q"
  " 1 or 0 -> blinking block
  " 3 -> blinking underscore
  " 4 -> solid underscore
  " Recent versions of xterm (282 or above) also support
  " 5 -> blinking vertical bar
  " 6 -> solid vertical bar
