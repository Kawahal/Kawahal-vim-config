# Pedro Domingo's vim and gvim configuration 

## Plugins: 

- **[Pathogen](https://github.com/tpope/vim-pathogen)**
    Manage your 'runtimepath' with ease. In practical terms, pathogen.vim makes
    it super easy to install plugins and runtime files in their own private directories.

- **[NERDTree](https://github.com/scrooloose/nerdtree)**
    The NERD tree allows you to explore your filesystem and to open files and
    directories. It presents the filesystem to you in the form of a tree which
    you manipulate with the keyboard and/or mouse.

- **[Autoclose](https://github.com/Townk/vim-autoclose)**
    This plugin for Vim enable an auto-close chars feature for you. 
    For instance if you type an '(', autoclose will automatically insert 
    a ')' and put the cursor between than.  

- **[Vim-javascript](https://github.com/pangloss/vim-javascript)**
    Vastly improved Javascript indentation and syntax support in Vim. 

- **[YouCompleteMe](https://github.com/Valloric/YouCompleteMe)**
    A code-completion engine for Vim. 

- **[ALE (Asynchronous Lint Engine)](https://github.com/w0rp/ale)**
    ALE (Asynchronous Lint Engine) is a plugin for providing linting in NeoVim
    and Vim 8 while you edit your text files. 

- **[Airline](https://github.com/vim-airline/vim-airline)**
    Lean & mean status/tabline for vim that's light as air. 

- **[IndentLine](https://github.com/Yggdroot/indentLine)**
    A vim plugin to display the indention levels with thin vertical lines.

- **[Closetag](https://github.com/alvan/vim-closetag)**
    Auto close (X)HTML tags by pressing ">" or ">>". 

- **[Emmet-vim](https://github.com/mattn/emmet-vim)**
    Enables support for expanding abbreviations (HTML). 

- **[Vim-jsx](https://github.com/mxw/vim-jsx)**
    Syntax highlighting and indenting for JSX 

## Features: 

- Line number column at left 
- Adjusted font, fontsize and colors (slate colorscheme)
- Automatic bracket and pharentesis closing. 
- Tab and indenting width adjusted to 4. 
- Automattic completion (HTML files)
- Airline theme
- Automatic start of Nerdtree
- Line at 80 characters. 
- Automatic use of mouse. 
- Multiple other configurations. 
- Breezy colorscheme. 
- Configuration for React.js
